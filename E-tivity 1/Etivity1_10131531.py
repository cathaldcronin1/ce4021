"""
E-tivity 1 by Cathal Cronin
Student ID: 10131531
Date: 13/9/2018

Problem:

Define a function that takes a matrix size, a range of values to populate this matrix with
and return the sum of the diagonals and a fact about this number by calling a public API.

Requirements:

The Matrix will be a square and must be non negative.

The random values to populate the matrix must be defined as a tuple (min, max)
The values for these ranges cannot be negative.

When the matrix is populated, the function should sum both diagonals of the matrix.
Note - For odd sized matrices adding the center element twice is OK.
"""
import random
from urllib.request import urlopen

# Define Constants
API_URL = "http://numbersapi.com/"


def sum_matrix_diagonals(matrix_size, matrix_num_range=(1, 10)):
    """
    Given an integer N, returns the sum of the diagonals in that matrix
    and a fact about that number from an API.

    Args:
        matrix_size: (int) Size of matrix, must be >= 1
        matrix_num_range: (tuple) A tuple containing the min and max ranges of numbers to
        fill the matrix with. Defaults to (1,10) Min 1 Max 10

    Returns:
        A dict containing an int and a message.
        E.g result = {'-1': "ERROR: matrix_size must be >= 1"}
    """
    if matrix_size < 1:
        print("ERROR: matrix_size must be >= 1")
        result = {-1: "ERROR: matrix_size must be >= 1"}
    else:
        diag_sum = 0

        # Tuple values get assigned to the min and max
        min_rand_val, max_rand_val = matrix_num_range

        # Check tuples aren't negative, if so return error
        if min_rand_val < 0 or max_rand_val < 0:
            return {-2: 'ERROR: min max ranges are negative!'}

        # List comprehension to create an N x N matrix filled with random values between min and max
        matrix = [[random.randint(min_rand_val, max_rand_val) for j in range(matrix_size)] for i in range(matrix_size)]

        # Sum diagonals of the matrix
        print("Summing Diagonals of matrix")
        for i, row in enumerate(matrix):
            print("Row %s: %s" % (i, row))
            diag_sum += row[i]
            diag_sum += row[-1 - i]

        print("Sum of diagonal: %s" % diag_sum)

        # Make a request to an API to get a fact about the given number!
        api_request_url = API_URL + str(diag_sum)
        sum_fact = urlopen(api_request_url).read().decode("utf-8")
        print("\nHere is a fact about the number of the sum!\n\"%s\"" % sum_fact)

        result = {diag_sum: sum_fact}

    return result


if __name__ == "__main__":
    sum_matrix_diagonals(4)
