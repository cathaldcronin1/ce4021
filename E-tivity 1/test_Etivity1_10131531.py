import unittest

from .Etivity1_10131531 import sum_matrix_diagonals


class TestEtivity1(unittest.TestCase):

    def test_negative_matrix_size(self):
        matrix_size = -1
        expected_result = {-1: "ERROR: matrix_size must be >= 1"}

        result = sum_matrix_diagonals(matrix_size)
        self.assertEqual(result, expected_result)

    def test_matrix_sum(self):
        matrix_size = 3
        matrix_num_range = (1, 1)

        # API returns different facts about same numbers, harder to test the response.
        expected_result = {6: "6 is the number of fundamental flight instruments lumped together on a cockpit display."}

        result = sum_matrix_diagonals(matrix_size, matrix_num_range)
        self.assertEqual(result.keys(), expected_result.keys())

    def test_negative_matrix_fill_values(self):
        matrix_size = 3
        matrix_num_range = (-1, -10)

        expected_result = {-2: 'ERROR: min max ranges are negative!'}

        result = sum_matrix_diagonals(matrix_size, matrix_num_range)
        self.assertEqual(result, expected_result)


if __name__ == '__main__':
    unittest.main()
